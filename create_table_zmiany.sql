create table ZMIANY (
   id_zmiany INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
   data_zmiany DATE default CURRENT_DATE,
   czas_zmiany TIME default CURRENT_TIME,

   rodzaj_zmiany DECIMAL(3),
   id_buta INTEGER not null,
   producent VARCHAR(15) default NULL,
   typ  VARCHAR(10) default NULL,
   czy_wykonane_ze_skory BOOLEAN default NULL,

   PRIMARY KEY (id_zmiany)
);