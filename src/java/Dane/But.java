package Dane;

/**
 *
 * @author Mefju
 */

public class But {
    
    private int id;
    private String producent;
    private String typ;
    private boolean skora;

    public But(){
        
    }

    public But(String producent, String typ, boolean skora) {
        this.producent = producent;
        this.typ = typ;
        this.skora = skora;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProducent() {
        return producent;
    }

    public void setProducent(String producent) {
        this.producent = producent;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public boolean isSkora() {
        return skora;
    }

    public void setSkora(boolean skora) {
        this.skora = skora;
    }
    
}
