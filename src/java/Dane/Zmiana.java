package Dane;

import java.sql.Date;
import java.sql.Time;

/**
 * Klasa POJO przechowujaca informacje o kazdej zmianie w bazie danych
 * @author Mefju
 */

public class Zmiana {
    
    private int id;
    private Date data;
    private Time godzina;
    private int rodzaj;
    private int id_buta;
    private String producent;
    private String typ;
    private boolean skora;

    public Zmiana(){
        
    }

    public Zmiana(int rodzaj, But but) {
        this.data = new Date(System.currentTimeMillis());
        this.godzina = new Time(System.currentTimeMillis());
            
        this.rodzaj = rodzaj;
        this.id_buta = but.getId();
        this.producent = but.getProducent();
        this.typ = but.getTyp();
        this.skora = but.isSkora();
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProducent() {
        return producent;
    }

    public void setProducent(String producent) {
        this.producent = producent;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public boolean isSkora() {
        return skora;
    }

    public void setSkora(boolean skora) {
        this.skora = skora;
    }
    
    public int getId_buta() {
        return id_buta;
    }

    public void setId_buta(int id_buta) {
        this.id_buta = id_buta;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Time getGodzina() {
        return godzina;
    }

    public void setGodzina(Time godzina) {
        this.godzina = godzina;
    }

    public int getRodzaj() {
        return rodzaj;
    }

    public void setRodzaj(int rodzaj) {
        this.rodzaj = rodzaj;
    }
    
    
}
