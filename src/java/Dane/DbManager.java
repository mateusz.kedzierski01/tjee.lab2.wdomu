package Dane;
import java.util.ArrayList;
import java.util.List; 
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class DbManager {
    private static SessionFactory factory;
    
    public DbManager(){
        Configuration configuration = new Configuration();
        configuration.configure("/Trwalosc/hibernate.cfg.xml");
        ServiceRegistry  serviceRegistry = (ServiceRegistry) new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();        
        factory = configuration.buildSessionFactory(serviceRegistry);
    }
   
    
    public Integer dodaj(String producent, String typ, boolean skora){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer idButa = null;
        try {
            tx = session.beginTransaction();
            But but = new But(producent, typ, skora);
            idButa = (Integer) session.save(but); 
            Zmiana zmiana = new Zmiana(0, but);
            session.save(zmiana); 
            tx.commit();
        } catch(HibernateException e){
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally{
            session.close();
        }
        return idButa;
    }
   
   
    public String listuj( ){
        Session session = factory.openSession();
        Transaction tx = null;
        List<But> lista = new ArrayList<>();
        String wiersz = new String();
        wiersz = wiersz.concat("<table>");
        wiersz = wiersz.concat("<tr>");
        wiersz = wiersz.concat("<th>ID</th>"
                             + "<th>Producent</th>"
                             + "<th>Typ</th>"
                             + "<th>Wykonany ze skóry</th> ");
        wiersz = wiersz.concat("</tr> ");
      
        try {
            tx = session.beginTransaction();
            lista.addAll(session.createQuery("FROM But").list());
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close(); 
        }
        
        if(lista.isEmpty()) return "Baza jest pusta!<br/>";
        
        for(int i = 0; i<lista.size(); i++){
            But b = lista.get(i);
            wiersz = wiersz.concat("<tr>");
            wiersz = wiersz.concat("<td>" + b.getId() + "</td>");
            wiersz = wiersz.concat("<td>" + b.getProducent() + "</td>");
            wiersz = wiersz.concat("<td>" + b.getTyp() + "</td>");
            wiersz = wiersz.concat("<td>" + (b.isSkora() ? "tak" : "nie") + "</td>");
            wiersz = wiersz.concat("</tr>");
        }
        
        wiersz = wiersz.concat("</concat>");
        return wiersz;
    }
   
   
    public void usun(Integer idButa) throws HibernateException {
        Session session = factory.openSession();
        Transaction tx;
        tx = session.beginTransaction();
        But but = (But)session.get(But.class, idButa); 
        session.delete(but); 
        Zmiana zmiana = new Zmiana(1, but);
        session.save(zmiana); 
        tx.commit();
        session.close();
    }
    
    
    public String listaZmian( ){
        Session session = factory.openSession();
        Transaction tx = null;
        List<Zmiana> lista = new ArrayList<>();
        String wiersz = new String();
        try {
            tx = session.beginTransaction();
            lista.addAll(session.createQuery("FROM Zmiana").list());
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close(); 
        }
        
        if(lista.isEmpty()) {return "Brak zapisanych zmian<br/>";}
        
        wiersz = wiersz.concat(   "<table><tr><th></th>"
                                + "<th>Data</th>"
                                + "<th>Godzina</th>"
                                + "<th>Rodzaj zmiany</th>"
                                + "<th>ID buta</th>"
                                + "<th>Producent</th>"
                                + "<th>Typ</th>"
                                + "<th>Czy wykonany ze skory</th></tr>"
        );
        
        for(int i = 0; i<lista.size(); i++){
            Zmiana z = lista.get(i);
            wiersz = wiersz.concat("<tr>");
            wiersz = wiersz.concat("<td>" + z.getId() + "</td>");
            wiersz = wiersz.concat("<td>" + z.getData() + "</td>");
            wiersz = wiersz.concat("<td>" + z.getGodzina() + "</td>");
            wiersz = wiersz.concat("<td>" + (z.getRodzaj()==0 ? "Dodano" : "Usunięto") + "</td>");
            wiersz = wiersz.concat("<td>" + z.getId_buta() + "</td>");
            wiersz = wiersz.concat("<td>" + z.getProducent() + "</td>");
            wiersz = wiersz.concat("<td>" + z.getTyp() + "</td>");
            wiersz = wiersz.concat("<td>" + (z.isSkora() ? "tak" : "nie") + "</td>");
            wiersz = wiersz.concat("</tr>");
        }
        
        wiersz = wiersz.concat("</table>");
        return wiersz;
    }

}
   