package Serwlety;

import Dane.DbManager;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Mefju
 */
public class Usuwanie extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Usuwanie z bazy danych</title>");            
            out.println("<link rel=\"stylesheet\" href=\"Style/styl.css\">");            
            out.println("</head>");
            out.println("<body>");
            
            DbManager man = new DbManager();
            int id = 0;
            boolean correct = true;
            try{
                id = Integer.parseInt(request.getParameter("id"));
                man.usun(id);
            } catch(NumberFormatException e){
                out.println("<h1>Niepoprawne ID (" + request.getParameter("id") + ")<br/>");
                correct = false;
            } catch(IllegalArgumentException e){
                out.println("<h1>Nie znaleziono buta o podanym ID ("+ id +").</h1>");
                correct = false;
            }
            if(correct){
                out.println("<h1>Pomyślnie usunięto wpis o ID: " + id + ".</h1>");
            }
            
            out.println("<a class=\"button\" href=" + request.getHeader("referer") + ">Powrót</a>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
